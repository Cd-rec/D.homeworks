package com.company;

public class Main {

    public static void main(String[] args) {
        int money = 584351;
        int[] bank = {5000, 2000, 1000, 500, 200, 100, 50, 10, 5, 2, 1};
        int i = 0;
        while (money != 0) {
            int count = money / bank[i];
            money = money % bank[i];
            System.out.println(count + " купюр по " + bank[i] + " rub.");
            i++;
        }

    }
}
