public class Main {
    public static int getIndexBack (int number) {
        int count = 0;

        while (number != 0) {
            number /= 10;
            count++;
        }
        return count;
    }
    public static void main(String[] args){
        int result = getIndexBack(1111);
        System.out.println(result);
    }
}